<?php

declare(strict_types = 1);
/**
 * This file is form http://findcat.cn
 *
 * @link     http://findcat.cn
 * @email    1476982312@qq.com
 */
use Psr\Log\LogLevel;
use Hyperf\Contract\StdoutLoggerInterface;

return [
    // 生产环境使用 prod 值
    'app_env' => env('APP_ENV', 'dev'),
    // 是否使用注解扫描缓存
    'scan_cacheable'             => env('SCAN_CACHEABLE', false),
    'app_name'                   => env('APP_NAME', 'skeleton'),
    StdoutLoggerInterface::class => [
        'log_level' => [
            LogLevel::ALERT,
            LogLevel::CRITICAL,
            LogLevel::DEBUG,
            LogLevel::EMERGENCY,
            LogLevel::ERROR,
            LogLevel::INFO,
            LogLevel::NOTICE,
            LogLevel::WARNING,
        ],
    ],
];
